import * as Cookies from 'js-cookie';
import qs from 'querystring'

export const REQUEST_CONFIG = {
  // defaults to same origin
  origin: process.env.NODE_ENV == 'production'? "https://bespokeweb.studio" : 'http://localhost:8000',
}

export const ENDPOINT = {
}

export default async (uri, options = {}) => {
  const { body } = options

  const response = await fetch(`${ REQUEST_CONFIG.origin }${ uri }`, {
    method: body ? 'POST' : 'GET',
    headers: {
      'X-CSRFToken':  Cookies.get('csrftoken'),
      'content-type': body ? 'application/json' : null,
    },
    body: body ? JSON.stringify(body) : null,
    credentials: 'same-origin',
  })

  return response.json()
}
