import React,  {useEffect, useState} from 'react'
import './App.sass';
import Main from './components/main/main'

import { Provider, useDispatch } from "react-redux";

const App = () => {
  const [isSticky, setSticky] = useState(false);
  const [collapseMenu, setCollapseMenu] = useState(false);

  const dispatch = useDispatch()

  const handleScroll = () => {
    if (window.pageYOffset > 120) {
      setSticky(true)
    } else {
      setSticky(false)
    }
  }
  const [activeMenu, setActiveMenu] = useState("home");

  const clickMenu = (menu) => {
      console.log('click menu', menu)
      setActiveMenu(menu)
  }

  useEffect(() => {
    window.addEventListener('scroll', handleScroll);

    return () => {
      window.removeEventListener('scroll', () => handleScroll);
    };
  }, []);

  const goToMenu = (menu) => {
    clickMenu(menu)
    setCollapseMenu(false)
    // setTimeout( () => window.location.href = `/#${menu}`, 300)
  }

  if (collapseMenu) {
    return (
      <div className={`menu ${collapseMenu ? "visible":""}`}>
        <div className="top" onClick={() => setCollapseMenu(false)}>
            Close
        </div>
        <div className="menu-list">
          <div className={`menu-item ${activeMenu==="home" ? "active": ""}`} onClick={() => goToMenu("home")}><span>Home</span></div>
          <div className={`menu-item ${activeMenu==="skill" ? "active": ""}`} onClick={() => goToMenu("skill")}><span>Skills</span></div>
          <div className={`menu-item ${activeMenu==="how" ? "active": ""}`} onClick={() => goToMenu("how")}><span>How</span></div>
          <div className={`menu-item ${activeMenu==="contact" ? "active": ""}`} onClick={() => goToMenu("contact")}><span>Contact</span></div>
        </div>
      </div>
    )
  }

  return (
    <div className="App">
      <Main sticky={isSticky} setCollapseMenu={setCollapseMenu} clickMenu={clickMenu} activeMenu={activeMenu}/>
    </div>
  )
}


export default App;
