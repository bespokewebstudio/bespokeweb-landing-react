import React, {useEffect, useState} from 'react';
import './header.sass'
import './main.sass'
import Particles from 'react-particles-js';

const randomExcept = (choices, except) => {
    const filtered = choices.filter(x => x !== except)
    return filtered[Math.floor(Math.random() * filtered.length)];
}

const Button = ({title}) => {
    const [hover, setHover] = useState(false);

    const onHover = () => {
        setHover(true)
    }
    const onOut = () => {
        setHover(false)
    }
    return (
        <a href="mailto:contact@bespokeweb.studio" className={`action-btn ${hover?"hover": ""}`} onMouseOver={onHover} onMouseOut={onOut}>
            <i><svg width="258" height="50" xmlns="http://www.w3.org/2000/svg"><path stroke="#FFF" strokeWidth="2" d="M1 1h256v48H1z" fill="none" fillRule="evenodd"></path></svg>
            </i>
            {title}
        </a>
    )
}

function FadeInSection(props) {
    const [isVisible, setVisible] = React.useState(false);
    const domRef = React.useRef();
    React.useEffect(() => {
      const observer = new IntersectionObserver(entries => {
        entries.forEach(entry => setVisible(entry.isIntersecting));
      });
      observer.observe(domRef.current);
    }, []);
    return (
      <div
        className={`fade-in-section ${isVisible ? 'is-visible' : ''}`}
        ref={domRef}
      >
        {props.children}
      </div>
    );
  }

const PARAM = {
    particles: {
      number: { value: 80, density: { enable: true, value_area: 800 } },
      color: { value: "#ffffff" },
      shape: {
        type: "circle",
        stroke: { width: 0, color: "#000000" },
        polygon: { nb_sides: 5 },
        image: { src: "img/github.svg", width: 100, height: 100 }
      },
      opacity: {
        value: 0.5,
        random: false,
        anim: { enable: false, speed: 1, opacity_min: 0.1, sync: false }
      },
      size: {
        value: 3,
        random: true,
        anim: { enable: false, speed: 40, size_min: 0.1, sync: false }
      },
      line_linked: {
        enable: true,
        distance: 150,
        color: "#ffffff",
        opacity: 0.4,
        width: 1
      },
      move: {
        enable: true,
        speed: 6,
        direction: "none",
        random: false,
        straight: false,
        out_mode: "out",
        bounce: false,
        attract: { enable: false, rotateX: 600, rotateY: 1200 }
      }
    },
    interactivity: {
      detect_on: "canvas",
      events: {
        onhover: { enable: true, mode: "repulse" },
        onclick: { enable: true, mode: "push" },
        resize: true
      },
      modes: {
        grab: { distance: 400, line_linked: { opacity: 1 } },
        bubble: { distance: 400, size: 40, duration: 2, opacity: 8, speed: 3 },
        repulse: { distance: 200, duration: 0.4 },
        push: { particles_nb: 4 },
        remove: { particles_nb: 2 }
      }
    },
    retina_detect: true
  }

const Main = ({sticky, setCollapseMenu, clickMenu, activeMenu}) => {
    useEffect(() => {
        window.location.href = `/#${activeMenu}`
    }, [activeMenu])

    return (
        <React.Fragment>
        <header className={`header ${sticky ? "sticky": ""}`}>
            <div className="header-inner">
                <a className="left" href="#">
                    <div className="logo">
                        <svg id="Capa_1" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m300 406.299v-89.308l78.703-45.013 78.703 45.013v89.308l-78.703 45.013z" fill="#fff59b"/></g><g><path d="m506.371 282.787-117.668-67.297v-135.312c0-3.587-1.921-6.899-5.035-8.681l-122.703-70.177c-3.076-1.76-6.853-1.76-9.93 0l-122.702 70.178c-3.114 1.781-5.035 5.094-5.035 8.681v135.311l-117.668 67.297c-3.114 1.781-5.035 5.094-5.035 8.681v140.355c0 3.587 1.921 6.899 5.035 8.681l122.703 70.178c1.538.88 3.251 1.319 4.965 1.319 1.713 0 3.427-.44 4.965-1.319l117.737-67.339 117.738 67.338c1.538.88 3.251 1.319 4.965 1.319s3.427-.44 4.965-1.319l122.703-70.178c3.114-1.781 5.035-5.094 5.035-8.681v-140.355c0-3.587-1.921-6.899-5.035-8.68zm-260.371 143.235-112.703 64.459-112.703-64.459v-128.754l112.703-64.458 112.703 64.458zm-102.702-210.532v-129.511l112.702-64.459 112.703 64.459v129.511l-112.703 64.457zm348.108 210.532-112.703 64.459-112.703-64.459v-128.754l112.703-64.458 112.703 64.458z" fill="#020288"/></g></g></svg>
                    </div>
                    <div className="slogan">BespokeWeb</div>
                </a>
                <nav>
                    <ul className="navigation-list">
                        <li className="navigation-item" onClick={() => clickMenu("home")}>
                            <a className={`${activeMenu==="home" ? "active": ""}`} href="#"><span>Home</span></a>
                        </li>
                        <li className="navigation-item" onClick={() => clickMenu("skill")}>
                            <a className={`${activeMenu==="skill" ? "active": ""}`} href="#skill"><span>Skills</span></a>
                        </li>
                        <li className="navigation-item" onClick={() => clickMenu("how")}>
                            <a className={`${activeMenu==="how" ? "active": ""}`} href="#how"><span>How</span></a>
                        </li>
                        <li className="navigation-item" onClick={() => clickMenu("contact")}>
                            <a className={`${activeMenu==="contact" ? "active": ""}`} href="#contact"><span>Contact Us</span></a>
                        </li>
                    </ul>
                </nav>
                <a className="menu-collapse" onClick={() => setCollapseMenu(true)}>
                    <span>Menu</span>
                </a>
            </div>
        </header>
        <div className="main">
            <div className="landing">
                <div className="particle"><Particles params={PARAM}/></div>
                <div className="introduction">
                    <div>Seeking a balance between impact, excellence­ and pragmatism.</div>
                    <div className="last-child">We are Bespoke Web Studio</div>
                    <div className="last-child">We do Tailored-Made Web Design</div>
                </div>
            </div>
            <div className="skill" id="skill">
                <div className="section-title"><span>Skills</span></div>
                <div className="skill-detail">
                    <h3>As an interdisciplinary studio, we specialise in designing and developing:</h3>
                    <div className="skill-combine">
                        <div className="skill-combine-text-change skill-combine-left" >
                            <div className="first-child">Experiences</div>
                            <div>Websites</div>
                            <div>Applications</div>
                        </div>
                        <div className="skill-combine-middle">using</div>
                        <div className="skill-combine-text-change skill-combine-right">
                            <div>WebGL</div>
                            <div>Python</div>
                            <div>React</div>
                            <div>GraphQL</div>
                            <div className="first-child">Node.js</div>
                        </div>
                    </div>
                    <h3>With focus on user experience, quality and scalability.</h3>
                </div>
            </div>
            <div className="how" id="how">
                <div className="how-inner">
                    <div className="section-title"><span>How?</span></div>
                    <div className="statement-list">
                        <FadeInSection>
                        <div className="statement">
                            <div className="statement-text-group">
                                <div className="statement-title">Highly collaborative</div>
                                <div className="statement-content">Continuous flow of knowledge and ideas through the team.</div>
                            </div>
                            <div className="statement-image">
                                <svg version="1.1" id="highly-collaborative" xmlns="http://www.w3.org/2000/svg" x="0" y="0" viewBox="0 0 624 523" ><g id="highly-collaborative-inline__cube_5___34Bqw"><g id="highly-collaborative-inline__track_5___2RhH1"><linearGradient id="highly-collaborative-inline__SVGID_1____1UCHM" gradientUnits="userSpaceOnUse" x1="2762.873" y1="-1558.86" x2="2698.342" y2="-1660.24" gradientTransform="matrix(.5063 .8808 .867 -.4983 182.822 -2839.72)"><stop offset=".041" stopColor="#2932d2" stopOpacity="0"></stop><stop offset="1" stopColor="#2932d2"></stop></linearGradient><path fill="url(#highly-collaborative-inline__SVGID_1____1UCHM)" d="M109.4 368.6l121-69.5-.1 67.1-120.5 69.6z"></path><linearGradient id="highly-collaborative-inline__SVGID_2____1fGkl" gradientUnits="userSpaceOnUse" x1="-2490.699" y1="-1571.659" x2="-2576.244" y2="-1644.533" gradientTransform="matrix(-.4903 -.853 .867 -.4983 294.592 -2645.183)"><stop offset=".041" stopColor="#2932d2" stopOpacity="0"></stop><stop offset="1" stopColor="#2932d2"></stop></linearGradient><path fill="url(#highly-collaborative-inline__SVGID_2____1fGkl)" d="M110.4 368l121-69.5-57.1-32.2-120.6 69.4z"></path></g><linearGradient id="highly-collaborative-inline__SVGID_3____1Pbbb" gradientUnits="userSpaceOnUse" x1="1.029" y1="735.851" x2="111.759" y2="749.447" gradientTransform="translate(0 -376)"><stop offset="0" stopColor="#241ba9"></stop><stop offset="1" stopColor="#352062"></stop></linearGradient><path fill="url(#highly-collaborative-inline__SVGID_3____1Pbbb)" d="M.2 366.6l56.2 32.5 56.2-32.5-56.2-32.4z"></path><linearGradient id="highly-collaborative-inline__SVGID_4____2WnUj" gradientUnits="userSpaceOnUse" x1="84.5" y1="843.1" x2="84.5" y2="742.6" gradientTransform="translate(0 -376)"><stop offset="0" stopColor="#241b69"></stop><stop offset="1" stopColor="#352062"></stop></linearGradient><path fill="url(#highly-collaborative-inline__SVGID_4____2WnUj)" d="M56.4 399.1v68l56.2-32.9v-67.6z"></path><linearGradient id="highly-collaborative-inline__SVGID_5____3ZG59" gradientUnits="userSpaceOnUse" x1="51.599" y1="792.85" x2="107.799" y2="792.85" gradientTransform="matrix(-1 0 0 1 108 -376)"><stop offset="0" stopColor="#2932d2"></stop><stop offset="1" stopColor="#2932d2"></stop></linearGradient><path fill="url(#highly-collaborative-inline__SVGID_5____3ZG59)" d="M56.4 399.1v68L.2 434.2v-67.6z"></path></g><g id="highly-collaborative-inline__cube_2___S2GL0"><linearGradient id="highly-collaborative-inline__SVGID_6____30MIV" gradientUnits="userSpaceOnUse" x1="427.193" y1="731.296" x2="571.419" y2="749.005" gradientTransform="translate(0 -376)"><stop offset="0" stopColor="#241ba9"></stop><stop offset="1" stopColor="#352062"></stop></linearGradient><path fill="url(#highly-collaborative-inline__SVGID_6____30MIV)" d="M426.1 364.2l73.2 42.2 73.2-42.2-73.2-42.3z"></path><linearGradient id="highly-collaborative-inline__SVGID_7____lZokS" gradientUnits="userSpaceOnUse" x1="535.9" y1="871" x2="535.9" y2="740.2" gradientTransform="translate(0 -376)"><stop offset="0" stopColor="#241b69"></stop><stop offset="1" stopColor="#352062"></stop></linearGradient><path fill="url(#highly-collaborative-inline__SVGID_7____lZokS)" d="M499.3 406.4V495l73.2-42.8v-88z"></path><linearGradient id="highly-collaborative-inline__SVGID_8____2XvnM" gradientUnits="userSpaceOnUse" x1="-354.701" y1="871" x2="-354.701" y2="740.2" gradientTransform="matrix(-1 0 0 1 108 -376)"><stop offset="0" stopColor="#2932d2"></stop><stop offset="1" stopColor="#2932d2"></stop></linearGradient><path fill="url(#highly-collaborative-inline__SVGID_8____2XvnM)" d="M499.3 406.4V495l-73.2-42.8v-88z"></path><g id="highly-collaborative-inline__track_2___Ui-p_"><linearGradient id="highly-collaborative-inline__SVGID_9____rHrK6" gradientUnits="userSpaceOnUse" x1="5305.857" y1="2974.81" x2="5210.315" y2="2802.448" gradientTransform="matrix(-.5063 .8808 -.867 -.4983 5495.986 -2839.72)"><stop offset=".041" stopColor="#2932d2" stopOpacity="0"></stop><stop offset="1" stopColor="#2932d2" stopOpacity=".9"></stop></linearGradient><path fill="url(#highly-collaborative-inline__SVGID_9____rHrK6)" d="M426.3 364.3L232.9 252.1l.1 88.4 193.3 111.8z"></path><linearGradient id="highly-collaborative-inline__SVGID_10____1DeaX" gradientUnits="userSpaceOnUse" x1="-5072.725" y1="2968.697" x2="-5178.436" y2="2809.227" gradientTransform="matrix(.4903 -.853 -.867 -.4983 5384.216 -2645.183)"><stop offset=".041" stopColor="#2932d2" stopOpacity="0"></stop><stop offset=".296" stopColor="#2b2eb7" stopOpacity=".266"></stop><stop offset=".842" stopColor="#312572" stopOpacity=".836"></stop><stop offset="1" stopColor="#33225d"></stop></linearGradient><path fill="url(#highly-collaborative-inline__SVGID_10____1DeaX)" d="M425.9 364.2L233.2 251.8l73.8-41.3 192.1 111.4z"></path></g></g><g id="highly-collaborative-inline__cube_4___2N0HR"><g id="highly-collaborative-inline__track_4___3XyPp"><linearGradient id="highly-collaborative-inline__SVGID_11____3f2lO" gradientUnits="userSpaceOnUse" x1="2779.675" y1="-1116.219" x2="2696.127" y2="-1247.478" gradientTransform="matrix(.5063 .8808 .867 -.4983 182.822 -2839.72)"><stop offset=".126" stopColor="#fd5557" stopOpacity="0"></stop><stop offset="1" stopColor="#e92850"></stop></linearGradient><path fill="url(#highly-collaborative-inline__SVGID_11____3f2lO)" d="M466.1 162.1l156.6-89.9-.1 87.6-156.1 89.5z"></path><linearGradient id="highly-collaborative-inline__SVGID_12____27eoE" gradientUnits="userSpaceOnUse" x1="-2566.835" y1="-1246.318" x2="-2480.278" y2="-1115.743" gradientTransform="matrix(-.4903 -.853 .867 -.4983 294.592 -2645.183)"><stop offset="0" stopColor="#e7244f"></stop><stop offset=".879" stopColor="#fd5557" stopOpacity="0"></stop></linearGradient><path fill="url(#highly-collaborative-inline__SVGID_12____27eoE)" d="M466.1 162.2L624 71.4l-74.5-41.9L392.1 120z"></path></g><linearGradient id="highly-collaborative-inline__SVGID_13____2smT0" gradientUnits="userSpaceOnUse" x1="323.495" y1="527.484" x2="467.917" y2="545.217" gradientTransform="translate(0 -376)"><stop offset="0" stopColor="#d4244f"></stop><stop offset="1" stopColor="#fd5557"></stop></linearGradient><path fill="url(#highly-collaborative-inline__SVGID_13____2smT0)" d="M322.4 160.4l73.3 42.3 73.3-42.3-73.3-42.4z"></path><linearGradient id="highly-collaborative-inline__SVGID_14____36bqb" gradientUnits="userSpaceOnUse" x1="432.3" y1="667.1" x2="432.3" y2="536.4" gradientTransform="translate(0 -376)"><stop offset="0" stopColor="#d4244f"></stop><stop offset="1" stopColor="#fd5557"></stop></linearGradient><path fill="url(#highly-collaborative-inline__SVGID_14____36bqb)" d="M395.6 202.7v88.4l73.4-42.9v-87.8z"></path><linearGradient id="highly-collaborative-inline__SVGID_15____1LeDq" gradientUnits="userSpaceOnUse" x1="-287.701" y1="601.95" x2="-214.401" y2="601.95" gradientTransform="matrix(-1 0 0 1 108 -376)"><stop offset="0" stopColor="#d4244f"></stop><stop offset="1" stopColor="#d71c44"></stop></linearGradient><path fill="url(#highly-collaborative-inline__SVGID_15____1LeDq)" d="M395.7 202.7v88.8l-73.3-43v-88.1z"></path></g><g id="highly-collaborative-inline__cube_1___2mKTc"><g id="highly-collaborative-inline__track_1___1VL3i"><linearGradient id="highly-collaborative-inline__SVGID_16____DDarM" gradientUnits="userSpaceOnUse" x1="5276.848" y1="3096.015" x2="5181.513" y2="2924.026" gradientTransform="matrix(-.5063 .8808 -.867 -.4983 5495.986 -2839.72)"><stop offset="0" stopColor="#2932d2" stopOpacity="0"></stop><stop offset="1" stopColor="#2932d2"></stop></linearGradient><path fill="url(#highly-collaborative-inline__SVGID_16____DDarM)" d="M335.9 277.4L142.5 166.8l.1 87.2 192.9 111.7z"></path><linearGradient id="highly-collaborative-inline__SVGID_17____3sNSF" gradientUnits="userSpaceOnUse" x1="-5036.266" y1="3198.292" x2="-5154.801" y2="2970.511" gradientTransform="matrix(.4903 -.853 -.867 -.4983 5384.216 -2645.183)"><stop offset=".041" stopColor="#2932d2" stopOpacity="0"></stop><stop offset="1" stopColor="#2932d2"></stop></linearGradient><path fill="url(#highly-collaborative-inline__SVGID_17____3sNSF)" d="M299.2 256.4L50.9 114.5l74.2-41.7 247.4 141.4z"></path></g><linearGradient id="highly-collaborative-inline__SVGID_18____3cvUI" gradientUnits="userSpaceOnUse" x1="300.188" y1="616.424" x2="413.911" y2="630.388" gradientTransform="translate(0 -376)"><stop offset="0" stopColor="#241ba9"></stop><stop offset="1" stopColor="#352062"></stop></linearGradient><path fill="url(#highly-collaborative-inline__SVGID_18____3cvUI)" d="M298.3 255.8l44.4 25.5 73.1-42.3-44.5-25.5z"></path><path fill="#e6e7e8" d="M342.8 281.2v88.9l73-43.1v-88z"></path><linearGradient id="highly-collaborative-inline__SVGID_19____3wx1u" gradientUnits="userSpaceOnUse" x1="-212.501" y1="746" x2="-212.501" y2="631.8" gradientTransform="matrix(-1 0 0 1 108 -376)"><stop offset="0" stopColor="#2932d2"></stop><stop offset="1" stopColor="#2932d2"></stop></linearGradient><path fill="url(#highly-collaborative-inline__SVGID_19____3wx1u)" d="M342.7 281.3V370l-44.4-26.2v-88z"></path></g><g id="highly-collaborative-inline__cube_3___3aWNB"><linearGradient id="highly-collaborative-inline__SVGID_20____1ciB2" gradientUnits="userSpaceOnUse" x1="5362.569" y1="3173.371" x2="5283.284" y2="3053.454" gradientTransform="matrix(-.5063 .8808 -.867 -.4983 5495.986 -2839.72)"><stop offset="0" stopColor="#fd5557" stopOpacity="0"></stop><stop offset=".515" stopColor="#f23c53" stopOpacity=".515"></stop><stop offset="1" stopColor="#e92850"></stop></linearGradient><path fill="url(#highly-collaborative-inline__SVGID_20____1ciB2)" d="M174.3 298.1L29.4 215l.1 80.7 144.4 83.9z"></path><linearGradient id="highly-collaborative-inline__SVGID_21____1T9ql" gradientUnits="userSpaceOnUse" x1="-5240.856" y1="3055.924" x2="-5156.117" y2="3172.557" gradientTransform="matrix(.4903 -.853 -.867 -.4983 5384.216 -2645.183)"><stop offset="0" stopColor="#e7244f"></stop><stop offset=".582" stopColor="#f33f53" stopOpacity=".418"></stop><stop offset="1" stopColor="#fd5557" stopOpacity="0"></stop></linearGradient><path fill="url(#highly-collaborative-inline__SVGID_21____1T9ql)" d="M174.4 298.2L28.2 214.3l68.6-38.6L242 259.2z"></path><linearGradient id="highly-collaborative-inline__SVGID_22____FrD7w" gradientUnits="userSpaceOnUse" x1="146.013" y1="649.15" x2="279.5" y2="665.54" gradientTransform="translate(0 -376)"><stop offset="0" stopColor="#241ba9"></stop><stop offset="1" stopColor="#352062"></stop></linearGradient><path fill="url(#highly-collaborative-inline__SVGID_22____FrD7w)" d="M145 281.4l67.8 39 67.7-39-67.7-39.1z"></path><linearGradient id="highly-collaborative-inline__SVGID_23____3mwIp" gradientUnits="userSpaceOnUse" x1="246.65" y1="778.4" x2="246.65" y2="657.4" gradientTransform="translate(0 -376)"><stop offset="0" stopColor="#241b69"></stop><stop offset="1" stopColor="#352062"></stop></linearGradient><path fill="url(#highly-collaborative-inline__SVGID_23____3mwIp)" d="M212.8 320.4v82l67.7-39.6v-81.4z"></path><linearGradient id="highly-collaborative-inline__SVGID_24____3yKhg" gradientUnits="userSpaceOnUse" x1="-70.901" y1="778.4" x2="-70.901" y2="657.4" gradientTransform="matrix(-1 0 0 1 108 -376)"><stop offset="0" stopColor="#2932d2"></stop><stop offset="1" stopColor="#2932d2"></stop></linearGradient><path fill="url(#highly-collaborative-inline__SVGID_24____3yKhg)" d="M212.8 320.4v82L145 362.8v-81.4z"></path></g></svg>
                            </div>
                        </div>
                        </FadeInSection>
                        <FadeInSection>
                        <div className="statement statement-reverse">
                            <div className="statement-image">
                                <svg version="1.1" id="open-source-driven" xmlns="http://www.w3.org/2000/svg" x="0" y="0" viewBox="0 0 556 555" ><g id="open-source-driven-inline__bottom___27BOa"><linearGradient id="open-source-driven-inline__SVGID_1____zKKl5" gradientUnits="userSpaceOnUse" x1="456.635" y1="258.452" x2="98.931" y2="258.452"><stop offset="0" stopColor="#d4244f"></stop><stop offset="1" stopColor="#fd5557"></stop></linearGradient><path fill="url(#open-source-driven-inline__SVGID_1____zKKl5)" d="M277.6 154.8L98.9 259.2l179.3 102.9 178.4-104.2z"></path><linearGradient id="open-source-driven-inline__SVGID_2____2IbKO" gradientUnits="userSpaceOnUse" x1="138.812" y1="467.713" x2="225.419" y2="329.113"><stop offset="0" stopColor="#2932d2" stopOpacity="0"></stop><stop offset="1" stopColor="#01169f" stopOpacity=".5"></stop></linearGradient><path fill="url(#open-source-driven-inline__SVGID_2____2IbKO)" d="M98.9 259.2L.7 377.9 278.5 555l-.3-192.9z"></path><linearGradient id="open-source-driven-inline__SVGID_3____2bwTZ" gradientUnits="userSpaceOnUse" x1="4809.885" y1="474.71" x2="4893.705" y2="323.494" gradientTransform="matrix(-1 0 0 1 5232 0)"><stop offset="0" stopColor="#241b69" stopOpacity="0"></stop><stop offset="1" stopColor="#352062" stopOpacity=".7"></stop></linearGradient><path fill="url(#open-source-driven-inline__SVGID_3____2bwTZ)" d="M456.6 257.9l99.6 120-277.7 176.4-.3-192.2z"></path></g><g id="open-source-driven-inline__middle___U6Y7M"><linearGradient id="open-source-driven-inline__SVGID_4____3ojCc" gradientUnits="userSpaceOnUse" x1="188.673" y1="362.611" x2="188.673" y2="150.271"><stop offset="0" stopColor="#241ba9"></stop><stop offset=".298" stopColor="#251eae"></stop><stop offset=".646" stopColor="#2626bc"></stop><stop offset=".999" stopColor="#2932d2"></stop></linearGradient><path fill="url(#open-source-driven-inline__SVGID_4____3ojCc)" d="M190.2 150.3L98.9 259.6l179.5 103V201.2z"></path><linearGradient id="open-source-driven-inline__SVGID_5____1-yO9" gradientUnits="userSpaceOnUse" x1="4864.657" y1="362.407" x2="4864.657" y2="149.617" gradientTransform="matrix(-1 0 0 1 5232 0)"><stop offset="0" stopColor="#2932d2" stopOpacity="0"></stop><stop offset="1" stopColor="#2932d2"></stop></linearGradient><path fill="url(#open-source-driven-inline__SVGID_5____1-yO9)" d="M367.4 149.6L456.2 258 278.7 362.4l-.3-161.2z"></path><path fill="#e6e7e8" d="M190 150.3l88.2 51 89-51.5-89-50.4z"></path></g><g id="open-source-driven-inline__top___1v0T0"><linearGradient id="open-source-driven-inline__SVGID_6____1YzKf" gradientUnits="userSpaceOnUse" x1="233.709" y1="158.885" x2="233.709" y2="-.183"><stop offset="0" stopColor="#d4244f"></stop><stop offset="1" stopColor="#fd5557"></stop></linearGradient><path fill="url(#open-source-driven-inline__SVGID_6____1YzKf)" d="M277.5-.2L189.9 108l87.6 50.9z"></path><linearGradient id="open-source-driven-inline__SVGID_7____1Ne_w" gradientUnits="userSpaceOnUse" x1="288.467" y1="-2.96" x2="325.819" y2="146.85"><stop offset=".003" stopColor="#fd5557" stopOpacity="0"></stop><stop offset="1" stopColor="#e92850"></stop></linearGradient><path fill="url(#open-source-driven-inline__SVGID_7____1Ne_w)" d="M277.5 158.9l89.8-51.2L277.5-.2z"></path></g></svg>
                            </div>
                            <div className="statement-text-group">
                                <div className="statement-title">Transversal Design</div>
                                <div className="statement-content">Continuous interaction between stakeholders coming from multiple horizons and having diverse, often opposite desires.</div>
                            </div>
                        </div>
                        </FadeInSection>
                    </div>
                </div>
            </div>
            <div className="contact" id="contact">
                <div className="action get-a-quote">
                    <div className="question">Estimate how we charge?</div>
                    <Button title="Get a quote"/>
                </div>
                <div className="action tell-us-more">
                    <div className="question">Looking for help with a project?</div>
                    <Button title="Tell us more"/>
                </div>
            </div>
            <footer className="footer">
                © 2020 BespokeWeb.studio
            </footer>
        </div>
        </React.Fragment>
    )
}

export default Main